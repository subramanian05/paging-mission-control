package hello;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import lombok.Data;
@Data
public class TelemetryService {
  private ObjectMapper mapper = new ObjectMapper();
  private List<TeleRecord> finalList = new ArrayList<>();

  public TelemetryService() {
    mapper.registerModule(new JodaModule());
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
  }

  public List<TeleRecord> process(String file) throws IOException {

    String fileName = new File(".").getAbsolutePath() + "/" + file;

    try (BufferedReader br = new BufferedReader(
        new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_8))) {

      String line;
      DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd HH:mm:ss.SSS");

      HashMap<String, List<TeleRecord>> h = new HashMap<>();

      while ((line = br.readLine()) != null) {
        StringTokenizer st = new StringTokenizer(line, "|");
        int fieldNumber = 0;
        TeleRecord tel = new TeleRecord();
        // <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
        while (st.hasMoreTokens()) {
          switch (fieldNumber) {
            case 0:
              DateTime dateTime = DateTime.parse(st.nextToken(), formatter);
              tel.setTimestamp(dateTime);
              break;
            case 1:
              tel.setSatelliteId(st.nextToken());
              break;
            case 2:
              tel.setRedHigh(Double.valueOf(st.nextToken()));
              break;
            case 3:
              st.nextToken();
              break;
            case 4:
              st.nextToken();
              break;
            case 5:
              tel.setRedLow(Double.valueOf(st.nextToken()));
            case 6:
              tel.setValue(Double.valueOf(st.nextToken()));
            case 7:
              tel.setComponent(st.nextToken());

          }
          fieldNumber++;
        }

        if (h.get(tel.getSatelliteId()) == null) {
          h.put(tel.getSatelliteId(), new ArrayList<TeleRecord>());
          h.get(tel.getSatelliteId()).add(tel);
        } else {
          h.get(tel.getSatelliteId()).add(tel);
        }

      }

      processHashMap(h);

      return finalList;

    }

  }

  private void processHashMap(HashMap<String, List<TeleRecord>> h) throws JsonProcessingException {

    for (Map.Entry<String, List<TeleRecord>> e : h.entrySet()) {
      List<TeleRecord> l = e.getValue();
      ArrayList<TeleRecord> batlist = new ArrayList<>();
      ArrayList<TeleRecord> tstatlist = new ArrayList<>();
      DateTime startBatTime = null;
      DateTime startTstatTime = null;

      for (TeleRecord tele : l) {
        if (tele.getValue() < tele.getRedLow() && tele.getComponent().equalsIgnoreCase("BATT")) {
          if (startBatTime == null) {
            startBatTime = tele.getTimestamp();
            batlist.add(tele);

          } else {
            long timeDiff = tele.getTimestamp().getMillis() - startBatTime.getMillis();
            if (timeDiff <= 5 * 60 * 1000) {
              if (batlist.size() < 3) {
                batlist.add(tele);
              } else if (batlist.size() == 3) {
                // output json using batlist.get(0)
                finalList.add(cloneWithSeverity(batlist.get(0), "RED LOW"));
                batlist.remove(0);
                startBatTime = batlist.get(0).getTimestamp();// get the new 0 index record

              } else {
                System.out.println("unhandled more than 3 scenario and gt than 5 min");

              }
            } else {
              if (batlist.size() < 3) {
                batlist.clear();
                batlist.add(tele);
                // no output json
                startBatTime = tele.getTimestamp();// get the new 0 index record

              } else if (batlist.size() == 3) {
                // output json using batlist.get(0)
                finalList.add(cloneWithSeverity(batlist.get(0), "RED LOW"));

                batlist.clear();
                batlist.add(tele);
                startBatTime = tele.getTimestamp();// get the new 0 index record
              } else {
                System.out.println("unhandled more than 3 scenario and gt than 5 min");
              }
            }
          }

        }

        if (tele.getValue() > tele.getRedHigh() && tele.getComponent().trim().equalsIgnoreCase("TSTAT")) {
          if (startTstatTime == null) {
            startTstatTime = tele.getTimestamp();
            tstatlist.add(tele);

          } else {
            long timeDiff = tele.getTimestamp().getMillis() - startTstatTime.getMillis();
            if (timeDiff <= 5 * 60 * 1000) {
              if (tstatlist.size() < 3) {
                tstatlist.add(tele);
              } else if (tstatlist.size() == 3) {
                // output json using tstatlist.get(0)
                finalList.add(cloneWithSeverity(tstatlist.get(0), "RED HIGH"));
                tstatlist.remove(0);
                startTstatTime = tstatlist.get(0).getTimestamp();// get the new 0 index record

              } else {
                System.out.println("unhandled more than 3 scenario and gt than 5 min");

              }
            } else {
              if (tstatlist.size() < 3) {
                tstatlist.clear();
                tstatlist.add(tele);
                // no output json
                startTstatTime = tele.getTimestamp();// get the new 0 index record

              } else if (tstatlist.size() == 3) {
                // output json using batlist.get(0)
                finalList.add(cloneWithSeverity(tstatlist.get(0), "RED HIGH"));
                tstatlist.clear();
                tstatlist.add(tele);
                startTstatTime = tele.getTimestamp();// get the new 0 index record
              } else {
                System.out.println("unhandled more than 3 scenario and gt than 5 min");
              }
            }
          }

        }

      }

      if (tstatlist.size() == 3) {
        finalList.add(cloneWithSeverity(tstatlist.get(0), "RED HIGH"));

      }
      if (batlist.size() == 3) {
        // output json using batlist.get(0)
        finalList.add(cloneWithSeverity(batlist.get(0), "RED LOW"));
      }

    }

    printJSON(finalList);

  }

  private  void printJSON(final List<TeleRecord> teleRecords) throws JsonProcessingException {
    String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(teleRecords);
    System.out.println(indented);

  }

  private  TeleRecord cloneWithSeverity(TeleRecord teleRecord, String severity) {
    TeleRecord t = new TeleRecord();
    t.setComponent(teleRecord.getComponent());
    t.setSeverity(severity);
    t.setSatelliteId(teleRecord.getSatelliteId());
    t.setTimestamp(teleRecord.getTimestamp());
    return t;
  }
}