package hello;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.joda.time.DateTime;

import lombok.Data;

@Data
@JsonPropertyOrder({ "satelliteId", "severity", "component", "timestamp" })
public class TeleRecord {
    /**
     * <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
     * The input that is parsed
     * */

     String satelliteId;
     String severity;
     String component;
     DateTime timestamp;

     @JsonIgnore
     Double redHigh;
     @JsonIgnore
     Double redLow;
     @JsonIgnore
     Double value;
     
}
