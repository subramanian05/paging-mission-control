package hello;

import java.io.IOException;

public class Telemetry {

  public static void main(String[] args) throws IOException {
    TelemetryService service =  new TelemetryService();
    service.process(args[0]);
  }

}