import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import hello.TelemetryService;

public class TelemetryServiceTest {
    
    @Test
    public void testProcessTest2() {
        TelemetryService t= new TelemetryService();
        try {
            t.process("test2.txt");
        } catch (IOException e) {
            fail("IO Error");
        }
        assertEquals(t.getFinalList().size(), 3);
    }

    @Test
    public void testProcessTest1() {
        TelemetryService t= new TelemetryService();
        try {
            t.process("test1.txt");
        } catch (IOException e) {
            fail("IO Error");
        }
        assertEquals(t.getFinalList().size(), 2);
    }

    @Test
    public void testProcessTest3() {
        TelemetryService t= new TelemetryService();
        try {
            t.process("test3.txt");
        } catch (IOException e) {
            fail("IO Error");
        }
        assertEquals(t.getFinalList().size(), 0);
    }

    @Test
    public void testProcessTest4() {
        TelemetryService t= new TelemetryService();
        try {
            t.process("test4.txt");
        } catch (IOException e) {
            fail("IO Error");
        }
        assertEquals(t.getFinalList().size(), 1);
    }
    
}
